//
// Created by diediaz on 10-09-18.
//

#ifndef OMNITIGSUNIBOSS_EDGE_BWT_HPP
#define OMNITIGSUNIBOSS_EDGE_BWT_HPP
#include <iostream>
#include <bitset>
#include <tuple>
#include "easylogging++.h"
#include <sdsl/sdsl_concepts.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/construct.hpp>
#include <sdsl/wavelet_trees.hpp>
#include <sdsl/wt_huff.hpp>


class rl_edge_bwt {
public:
    typedef uint64_t size_type;
    const sdsl::int_vector<64>& runs_acc = m_runs_acc;

private:
    friend class vo_boss;
    uint8_t m_sigma;
    sdsl::wt_huff<sdsl::rrr_vector<63>> m_h_symbols; // head symbols in the RL encoding
    sdsl::wt_huff<sdsl::rrr_vector<63>> m_f_symbols; // flagged symbols

    sdsl::rrr_vector<63>                m_dollars;   //dollars in the edge BWT
    sdsl::rrr_vector<63>                m_in_marks;  //marks to delimit incoming nodes
    sdsl::rrr_vector<63>::rank_1_type   m_dollars_rs;
    sdsl::rrr_vector<63>::rank_1_type   m_in_marks_rs;
    sdsl::rrr_vector<63>::select_0_type m_dollars_ss;
    sdsl::rrr_vector<63>::select_0_type m_in_marks_ss;

    //this will be replaced in the future with just two DTs
    sdsl::rrr_vector<63>  m_symbol_runs;
    sdsl::rrr_vector<63>  m_symbol_runs_sums;
    sdsl::rrr_vector<63>::rank_1_type m_symbol_runs_rs;
    sdsl::rrr_vector<63>::select_1_type m_symbol_runs_ss;
    sdsl::rrr_vector<63>::select_1_type m_symbol_runs_sums_ss;
    sdsl::rrr_vector<63>::rank_1_type m_symbol_runs_sums_rs;

    //the same idea of C array but for runs
    sdsl::int_vector<64> m_C={0,0,0,0,0,0,0};
    sdsl::int_vector<64> m_runs_acc={0,0,0,0,0,0,0};
    size_type bwt_size;

    //some temporal arrays to avoid calling the constructor so many times
    std::vector<uint8_t> tmp_symbols;
    std::vector<uint64_t> tmp_rank_i;
    std::vector<uint64_t> tmp_rank_j;


private:
    void copy(const rl_edge_bwt& other);

public:
    rl_edge_bwt();
    explicit rl_edge_bwt(sdsl::cache_config &config);
    size_type LFmapping(size_t index)const;
    //size_t edge_rank(size_t index, uint8_t symbol);
    size_type select(size_t node_rank, uint8_t symbol) const;
    uint8_t operator[](size_t index) const ;
    size_type range_symbols(size_t start_index, size_t end_index, std::vector<uint8_t>& symbols,
            std::vector<uint64_t>& rank_i, std::vector<uint64_t>& rank_j);
    bool has_one_symbol(size_t start_index, size_t end_index, uint8_t &symbol, size_t &rank_i, size_t &rank_j);
    bool same_symbol(const std::vector<size_type> &positions);

    size_type size()const;
    uint8_t pos2symbol(size_type kmer_pos) const;

    //storage functions
    size_type serialize(std::ostream& out, sdsl::structure_tree_node* v, std::string name)const;
    void load(std::istream& in);
    void swap(rl_edge_bwt&other);
    void print_dt_sizes();
};


#endif //OMNITIGSUNIBOSS_EDGE_BWT_HPP
