//
// Created by Diego Diaz on 3/19/18.
//

#ifndef OMNITIGSUNIBOSS_PARSEDNAFILES_HPP
#define OMNITIGSUNIBOSS_PARSEDNAFILES_HPP
extern "C"{
#include "kseq.h"
#include <zlib.h>
#include <cstdio>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
};
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sdsl/sd_vector.hpp>
#include "dna_alphabet.hpp"


class fast_x_parser {
    KSEQ_INIT(gzFile, gzread);
public:
    static int preproc_reads(std::string &input_file, sdsl::cache_config& config, size_t kmer_size);
};


#endif //OMNITIGSUNIBOSS_PARSEDNAFILES_HPP
