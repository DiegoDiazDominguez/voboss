//
// Created by Diego Diaz on 3/19/18.
//
#ifndef VOBOSS_DBG_HPP
#define VOBOSS_DBG_HPP

#include <iostream>
#include <fstream>
#include <bitset>

#include <sdsl/sdsl_concepts.hpp>
#include <sdsl/int_vector.hpp>
#include <sdsl/construct.hpp>
#include <sdsl/construct_lcp.hpp>
#include <sdsl/construct_sa.hpp>
#include <sdsl/construct_bwt.hpp>
#include <sdsl/wt_algorithm.hpp>
#include <sdsl/sd_vector.hpp>
#include <limits>

#include "wt_int.hpp"
#include "rl_edge_bwt.hpp"
#include "dna_alphabet.hpp"
#include "fast_x_parser.hpp"
#include "easylogging++.h"

class vo_boss{

private:
    typedef rrr_vector<63>                            t_comp_bv;
    typedef typename rrr_vector<63>::rank_0_type      t_rank_0;
    typedef typename rrr_vector<63>::select_0_type    t_select_0;
    typedef typename rrr_vector<63>::select_1_type    t_select_1;
    typedef wt_int_voboss<rrr_vector<63>>             t_lcs;

    size_t                                            m_k;
    size_t                                            m_m;
    size_t                                            m_n_solid_nodes=0;

    rl_edge_bwt                                       m_edge_bwt;
    t_lcs                                             m_lcs;
    t_comp_bv                                         m_node_marks;
    t_rank_0                                          m_node_marks_rs;
    t_select_0                                        m_node_marks_ss;

    t_comp_bv                                         m_solid_nodes;
    t_select_1                                        m_solid_nodes_ss;

public:
    typedef uint64_t                                  size_type;
    typedef std::pair<size_t, size_t>                 range_t;
    typedef std::pair<size_type, bool>                degree_t;

    struct node_t{
        size_t start;
        size_t end;
        size_t order;
        bool operator==(const node_t& other) {
            return (start==other.start && end==other.end && order==other.order);
        }

        bool operator!=(const node_t& other) {
            return (start!=other.start || end!=other.end || order!=other.order);
        }
    };

    const size_t &k                     =             m_k;
    const size_t &solid_kmers           =             m_n_solid_nodes;

    const t_comp_bv& node_marks         =             m_node_marks;
    const rl_edge_bwt& edge_bwt         =             m_edge_bwt;
    const t_comp_bv& solid_nodes        =             m_solid_nodes;
    const t_lcs&  lcs                   =             m_lcs;

    const t_rank_0& node_marks_rs       =             m_node_marks_rs;
    const t_select_0& node_marks_ss     =             m_node_marks_ss;
    const t_select_1& solid_nodes_ss    =             m_solid_nodes_ss;

public:
    //constructors
    vo_boss();
    vo_boss(std::string& input_file, cache_config& config, const size_t & K, const size_t & m);

    //navigational functions
    size_type dbg_outgoing(range_t v, uint8_t t) const;
    size_type dbg_outgoing(size_type v, uint8_t t) const;
    size_type dbg_incomming(size_type v, uint8_t t) const;
    node_t shorter(node_t v, size_t order) const;
    degree_t dbg_outdegree(size_type v) const;
    degree_t dbg_indegree(size_type v) const;
    std::vector<size_type> foverlaps(node_t v);

    //other functions
    node_t root()const;
    range_t get_edges(size_type v) const;
    std::vector<uint8_t> node_label(size_type v) const;
    std::vector<uint8_t> node_llabel(size_type v) const;
    size_type reverse_complement(size_type v);
    std::vector<std::pair<size_t, size_t>> backward_search(std::vector<uint8_t> &query, uint8_t mismatches);
    inline bool has_dollar_edges(range_t range) const;
    std::string node2string(size_type v, bool rev) const;

    //TODO I made them public just for testing
    node_t next_contained(node_t node);
    std::vector<size_type> buildL(node_t node);
    //

    //statistic functions
    inline size_type num_of_edges() const;
    inline size_type num_of_nodes() const;
    size_type num_of_dollar_edges() const;
    size_type num_of_marked_edges() const;

    //storage functions
    size_type serialize(std::ostream& out, structure_tree_node* v, std::string name)const;
    void load(std::istream&);

private:
    void build_sa_bwt_lcp(cache_config &config);
    void build_edgebwt(cache_config &config, size_t K);
    void build_klcp(cache_config &config, size_t K);
};

inline vo_boss::size_type vo_boss::dbg_outgoing(vo_boss::range_t v, uint8_t t) const {
    if(has_dollar_edges(v)) t++;
    assert(t>0 && t<=(v.second-v.first+1));
    return m_edge_bwt.LFmapping(std::get<0>(v)+ t-1);
}//t is the rank inside the range [1..\sigma]

inline vo_boss::size_type vo_boss::dbg_outgoing(vo_boss::size_type v, uint8_t t) const {
    range_t edges = get_edges(v);
    if(has_dollar_edges(edges)) t++;
    assert(t>0 && t<=(edges.second-edges.first+1));
    return m_edge_bwt.LFmapping(std::get<0>(edges)+ t-1);
}//t is the rank inside the range [1..\sigma]

inline vo_boss::size_type vo_boss::dbg_incomming(vo_boss::size_type v, uint8_t t) const{
    //TODO if the first is dollar, then move to the next as with outgoing
    assert(t>0);
    if(v==0) return 0;
    uint8_t symbol = m_edge_bwt.pos2symbol(v);
    size_t rank = v - m_edge_bwt.m_runs_acc[symbol-1];
    size_t edge_pos = m_edge_bwt.select(rank, symbol);

    if(t==1){
        return m_node_marks_rs.rank(edge_pos);
    }else{
        size_t next_edge_pos = m_edge_bwt.select(rank+1, symbol);

        size_t dollars_s = m_edge_bwt.m_dollars_rs.rank(edge_pos);
        size_t dollars_e = m_edge_bwt.m_dollars_rs.rank(next_edge_pos);

        size_t inv_s = m_edge_bwt.m_in_marks_rs.rank(edge_pos-dollars_s);
        size_t inv_e = m_edge_bwt.m_in_marks_rs.rank(next_edge_pos-dollars_e);

        size_t rank_i = m_edge_bwt.m_f_symbols.rank(inv_s, symbol);
        size_t rank_j = m_edge_bwt.m_f_symbols.rank(inv_e, symbol);

        assert((t-1U)<=(rank_j-rank_i));

        size_t symbol_rank = m_edge_bwt.m_f_symbols.select(rank_i+t-1, symbol)+1;
        size_t tmp = edge_pos-dollars_s;
        while(inv_s!=symbol_rank){
            if(m_edge_bwt.m_in_marks[tmp]){
                inv_s++;
            }
            tmp++;
        }

        return m_node_marks_rs.rank(tmp-1+dollars_s);
    }
}

inline vo_boss::node_t vo_boss::shorter(vo_boss::node_t v, size_t order) const{

    assert(v.order>=order);

    if (v.order < 1) return {0, num_of_edges()-1, 0};

    // search backward on WT to find the first occurrence of a number less than k
    size_t i = v.start;
    size_t j = v.end;

    // find largest i' <= i with L*[i' - 1] < k
    size_t i_prime = std::min(i, lcs.prev_lte(i+1, order-1)-1);
    // find smallest j' >= j with L*[j'] < k
    size_t j_prime = std::max(j,lcs.next_lte(j, order-1)-2);

    return {i_prime, std::max(i_prime, j_prime), order};
}

inline std::pair<vo_boss::size_type, bool> vo_boss::dbg_outdegree(vo_boss::size_type v) const {
    range_t range;
    bool has_dollar;
    range = get_edges(v);
    has_dollar = has_dollar_edges(range);
    return std::make_pair(range.second-range.first+1, has_dollar);
}

inline bool vo_boss::has_dollar_edges(vo_boss::range_t range) const{
    return (m_edge_bwt.m_dollars_rs.rank(range.second+1)-m_edge_bwt.m_dollars_rs.rank(range.first))!=0;
}

inline vo_boss::range_t vo_boss::get_edges(vo_boss::size_type v) const {
    size_t start, end;
    if(v==0){
        start=0;
    }else {
        start = m_node_marks_ss.select(v) + 1;
    }
    end = m_node_marks_ss.select(v+1);
    return std::make_pair(start, end);
}

inline std::vector<uint8_t> vo_boss::node_label(vo_boss::size_type v) const {
    std::vector<uint8_t> kmer(m_k-1);
    for(size_t i=0;i<m_k-1;i++){
        if(v==0){
            kmer[i] = 1; //only dollars
        }else {
            kmer[i] = m_edge_bwt.pos2symbol(v);
        }
        size_t rank = v - m_edge_bwt.m_runs_acc[kmer[i]-1];
        v = node_marks_rs.rank(m_edge_bwt.select(rank, kmer[i]));
    }
    return kmer;
}

inline std::string vo_boss::node2string(vo_boss::size_type v, bool rev) const {
    std::vector<uint8_t> kmer = node_label(v);
    std::string kmer_seq;

    for (unsigned char l : kmer) {
        kmer_seq.push_back(dna_alphabet::comp2char[l]);
    }

    if(rev){
        std::reverse(kmer_seq.begin(), kmer_seq.end());
    }
    return kmer_seq;
}

inline vo_boss::size_type vo_boss::num_of_edges() const {
    return m_edge_bwt.size();
}

inline vo_boss::size_type vo_boss::num_of_nodes() const {
    return m_edge_bwt.size();
}

inline vo_boss::size_type vo_boss::num_of_dollar_edges() const{
    return m_edge_bwt.m_dollars_rs(m_edge_bwt.size());
}

inline vo_boss::size_type vo_boss::num_of_marked_edges() const{
    return m_edge_bwt.m_f_symbols.size();
}

inline vo_boss::node_t vo_boss::root() const {
    return {0,num_of_nodes()-1,0};
}
#endif //VOBOSS_DBG_HPP
