//
// Created by Diego Diaz on 3/23/18.
//

#ifndef OMNITIGSUNIBOSS_DNA_ALPHABET_HPP
#define OMNITIGSUNIBOSS_DNA_ALPHABET_HPP

#include <sdsl/int_vector.hpp>

using namespace sdsl;

class dna_alphabet {

public:
    typedef int_vector<>::size_type     size_type;
    typedef int_vector<8>               char2comp_type;
    typedef int_vector<8>               comp2char_type;
    typedef int_vector<8>               rev_comp_type;
    typedef int_vector<64>              C_type;
    typedef uint16_t                    sigma_type;
    typedef uint8_t                     symbol_type;

    static const sigma_type             sigma;
    static const comp2char_type         comp2char;
    static const rev_comp_type          comp2rev;
    static const char2comp_type         char2comp;

    const C_type& C =                   m_C;

private:
    C_type m_C = {0, 0, 0, 0, 0, 0, 0};

public:
    dna_alphabet() = default;;
    explicit dna_alphabet(C_type& char_freqs):m_C(char_freqs){}
    size_type serialize(std::ostream& out, structure_tree_node* v=nullptr, std::string name="")const;
    void load(std::istream& in);
    void swap(dna_alphabet& in);
    symbol_type rank2char(size_t rank)const;
};


#endif //OMNITIGSUNIBOSS_DNA_ALPHABET_HPP
