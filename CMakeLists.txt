cmake_minimum_required(VERSION 3.7)
project(VOBOSS)

set (CMAKE_CXX_STANDARD 11)

set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -L${USER_LIB} -DELPP_FEATURE_PERFORMANCE_TRACKING")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

set(SOURCE_FILES
        src/main.cpp
        src/dna_alphabet.cpp
        src/fast_x_parser.cpp
        src/vo_boss.cpp
        src/easylogging++.cc
        src/rl_edge_bwt.cpp)

#create executable
add_executable(voboss ${SOURCE_FILES})

#compilation flags
target_compile_options(voboss
        PRIVATE
        -Werror -Wextra -msse4.2 -O3 -funroll-loops -fomit-frame-pointer -ffast-math)

#add include directories
list(APPEND includePath "${CMAKE_SOURCE_DIR}/include" ${USER_INCLUDE})
target_include_directories(voboss
        PRIVATE
        "$<BUILD_INTERFACE:${includePath}>")

#linking libraries
target_link_libraries(voboss PRIVATE sdsl divsufsort divsufsort64 z)

add_subdirectory(benchmarks)

