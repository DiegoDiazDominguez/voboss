# VOBOSS 

This is an unfinished implementation of the [VOBOSS](https://ieeexplore.ieee.org/document/7149295) data structure. We say unfinished
because there are a lot of things that can be improved yet. For instance, its construction.
We also tried to encode the edge symbols array (E array) using Run-Lenght encoding, but we did not get 
good results with real DNA sequencing data. Anyway, this implementation is intended to be compared with 
the data structure described [here](https://arxiv.org/abs/1901.10453).  

##Requisites
1. SDSL-lite library
2. CMake >=3.7

##Installation
```
$ git git clone https://DiegoDiazDominguez@bitbucket.org/DiegoDiazDominguez/voboss.git 
$ cd voboss 
$ mkdir build && cd build
$ cmake -DUSER_INCLUDE=/path/to/SDSL/include/dir -DLIB_INCLUDE=/path/to/SDSL/lib/dir ..
$ make
``` 

The binary files will be placed in the bin folder of the project home directory.


## How to use it  

Assuming you have a fastq file **my_fastq.fq**:

```
bin/voboss build my_fast.fq k min_ovp -o output_prefix
```
Where k is the maximum order for the variable-order de Bruijn graph and min_ovp is the minimum overlap allowed between vo-dBG nodes. To test
the functions described in the [paper](https://arxiv.org/abs/1901.10453) that were implemented in VO-BOSS just run

```
bin/vo_boss_bench output_prefix.voboss
```

## Contact

Any problem or question you can mail the first author of the [paper](https://arxiv.org/abs/1901.10453)


