//
// Created by Diego Diaz on 3/23/18.
//
#include "dna_alphabet.hpp"

const dna_alphabet::sigma_type dna_alphabet::sigma = 6;
const dna_alphabet::comp2char_type dna_alphabet::comp2char = {35, 36, 65, 67, 71, 84};
const dna_alphabet::rev_comp_type dna_alphabet::comp2rev = {0, 0, 5, 4, 3, 2};
const dna_alphabet::char2comp_type dna_alphabet::char2comp = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
                                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                            0, 0, 0, 0, 0, 2, 0, 3, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0,
                                                            0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 3,
                                                            0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5};

dna_alphabet::size_type dna_alphabet::serialize(std::ostream &out, structure_tree_node *v, std::string name)const
{
    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_type written_bytes = 0;
    written_bytes += m_C.serialize(out, child, "C");
    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

void dna_alphabet::load(std::istream &in)
{
    m_C.load(in);
}

void dna_alphabet::swap(dna_alphabet &in) {
    m_C.swap(in.m_C);
}

dna_alphabet::symbol_type dna_alphabet::rank2char(size_t rank) const {

    //TODO change this, it is awful
    if(rank<=m_C[2]){
       return 1;
    }else if(m_C[2]<rank && rank <=m_C[3]){
       return 2;
    }else if(m_C[3]<rank && rank <=m_C[4]){
       return 3;
    }else if(m_C[4]<rank && rank <=m_C[5]){
       return 4;
    }else{
       return 5;
    }
}
