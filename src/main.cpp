#define ELPP_NO_DEFAULT_LOG_FILE

#include <easylogging++.h>
#include <CLI11.hpp>
#include "vo_boss.hpp"

INITIALIZE_EASYLOGGINGPP

struct arguments{
    //common arguments
    std::string input_file;
    std::string tmp_dir;
    std::string id;

    //index construction arguments
    std::string output_index_prefix;
    size_t min_order;
    size_t k;
};

void setup_boss_cli(CLI::App *app, struct arguments& args){

    app->add_option("file", args.input_file,
                    "FASTA file with the input reads")->check(CLI::ExistingFile);
    app->add_option("K", args.k,
                    "K-mer size used for the index")->required();
    app->add_option("m", args.min_order,
                    "minimum overlap allowed")->required();
    app->add_option("-o,--output", args.output_index_prefix,
                    "Prefix for the output file")->set_default_val("dbg_index");
    app->add_option("-t,--tmp-dir", args.tmp_dir,
                    "Directory where temporal files will be placed")->set_default_val(".");
}

int main(int argc, char **argv){

    el::Configurations defaultConf;
    struct arguments args;

    CLI::App app("Variable-order dBG index");
    CLI::App *build = app.add_subcommand("build", "Build the variable order dBG index");

    setup_boss_cli(build, args);

    CLI11_PARSE(app, argc, argv);

    if(argc==1){
        std::cout<<app.help()<<std::endl;
        exit(EXIT_FAILURE);
    }

    if(app.got_subcommand(build)){
        //TODO K cannot be 0!
        if(argc==2){
            std::cout << build->help() << std::endl;
            exit(EXIT_FAILURE);
        }

        defaultConf.setGlobally(el::ConfigurationType::Format,
                                "%datetime [VO-BOSS-build] %level : %msg");
        el::Loggers::reconfigureLogger("default", defaultConf);

        LOG(INFO)<<"Building the VO-BOSS index for the input reads";
        cache_config config(false, args.tmp_dir, "tmp");
        {
            TIMED_SCOPE(timerObj, "VO-BOSS build");

            vo_boss hoboss(args.input_file, config, args.k, args.min_order);

            struct rusage r_usage;
            getrusage(RUSAGE_SELF, &r_usage);
            double memory_peak = 0;

#ifdef __APPLE__
            memory_peak = r_usage.ru_maxrss/(1024*1024);
#elif __linux__
            memory_peak = r_usage.ru_maxrss/(1024.0);
#endif
            std::ofstream os;
            double index_size = size_in_mega_bytes(hoboss);
            os.open(args.output_index_prefix+"_dbg_paper_stats");
            os<<"##max K: "+std::to_string(hoboss.k)+"\n";
            os<<"##tot solid nodes: "+std::to_string(hoboss.solid_kmers)+"\n";
            os<<"##tot edges: "+std::to_string(hoboss.num_of_edges())+"\n";
            os<<"##dollar edges: "+std::to_string(hoboss.num_of_dollar_edges())+"\n";
            os<<"##marked edges: "+std::to_string(hoboss.num_of_marked_edges())+"\n";
            os<<"##Index size: "+std::to_string(sdsl::size_in_mega_bytes(hoboss))+" MB"+"\n";
            os<<"###Run-length EdgeBWT: "+std::to_string((size_in_mega_bytes(hoboss.edge_bwt)/index_size)*100)+"%\n";
            os<<"###Node marks: "+std::to_string((size_in_mega_bytes(hoboss.node_marks)/index_size)*100)+"%\n";
            os<<"###Solid nodes marks: "+std::to_string((size_in_mega_bytes(hoboss.solid_nodes)/index_size)*100)+"%\n";
            os<<"##input_file: "+args.output_index_prefix+"\n";
            os<<"##memory_peak: "+std::to_string(memory_peak)+" MB\n";
            os<<"##elapsed_time: "+std::to_string(r_usage.ru_utime.tv_sec)+" seconds\n";
            os.close();
            store_to_file(hoboss, args.output_index_prefix+".voboss");
        }
        LOG(INFO)<< "Done!";
    }
    return EXIT_SUCCESS;
}