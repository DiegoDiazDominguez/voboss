//
// Created by Diego Diaz on 3/19/18.
//

#include <chrono>
#include <vo_boss.hpp>

using namespace std;
using namespace sdsl;

vo_boss::vo_boss(string &input_file, cache_config &config, const size_t & K, const size_t & m):
        m_k(K),m_m(m){

    fast_x_parser::preproc_reads(input_file, config, 1);

    //these functions build the index
    //TODO this is not the most efficient way to do it
    //We are working in a FAR better way to do it
    build_sa_bwt_lcp(config);
    build_klcp(config, K-1);
    build_edgebwt(config, K-1);
    //build the Run-length edgeBWT
    rl_edge_bwt ebwt(config);
    m_edge_bwt.swap(ebwt);
    //

    load_from_file(m_node_marks, cache_file_name("node_marks", config));
    m_node_marks_ss.set_vector(&m_node_marks);
    m_node_marks_rs.set_vector(&m_node_marks);

    load_from_file(m_solid_nodes, cache_file_name("solid_nodes", config));
    m_solid_nodes_ss.set_vector(&m_solid_nodes);

    sdsl::rrr_vector<63>::rank_1_type dollar_pref_rs;
    dollar_pref_rs.set_vector(&solid_nodes);

    //delete temporal files
    sdsl::util::delete_all_files(config.file_map);
    double index_size = size_in_mega_bytes(*this);
    LOG(INFO)<<"VO-BOSS index finished";
    LOG(INFO)<<"Stats:";
    LOG(INFO)<<" Maximum Kmer size: "+to_string(k);
    LOG(INFO)<<" Number of edges: "+to_string(num_of_edges());
    LOG(INFO)<<" The size of the index: "+to_string(index_size)+" MB";
    LOG(INFO)<<"  Run-length EdgeBWT: "+to_string((size_in_mega_bytes(m_edge_bwt)/index_size)*100)+"%";
    LOG(INFO)<<"  Node marks: "+to_string((size_in_mega_bytes(m_node_marks)/index_size)*100)+"%";
    LOG(INFO)<<"  Solid marks: "+to_string((size_in_mega_bytes(m_solid_nodes)/index_size)*100)+"%";
    LOG(INFO)<<"  LCS array:   "+to_string((size_in_mega_bytes(m_lcs)/index_size)*100)+"%";
}

void vo_boss::build_sa_bwt_lcp(cache_config &config){
    // construct SA
    LOG(INFO)<<"Building the SA array";
    construct_sa<8>(config);
    register_cache_file(conf::KEY_SA, config);

    // construct BWT
    LOG(INFO)<<"Building the BWT array";
    construct_bwt<8>(config);
    register_cache_file(conf::KEY_BWT, config);

    // construct LCP
    LOG(INFO)<<"Building the LCP array";
    construct_lcp_semi_extern_PHI(config);
    register_cache_file(conf::KEY_LCP, config);
}

vo_boss::size_type vo_boss::serialize(std::ostream &out, structure_tree_node *v, string name) const{

    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_t written_bytes = 0;
    written_bytes += m_lcs.serialize(out, child, "lcs");
    written_bytes += m_edge_bwt.serialize(out, child, "edge_bwt");
    written_bytes += m_node_marks.serialize(out, child, "node_marks");
    written_bytes += m_node_marks_rs.serialize(out, child, "node_marks_rs");
    written_bytes += m_node_marks_ss.serialize(out, child, "node_marks_ss");
    written_bytes += m_solid_nodes.serialize(out, child, "m_solid_nodes");
    written_bytes += m_solid_nodes_ss.serialize(out, child, "m_solid_nodes_ss");
    written_bytes += write_member(m_k, out, child, "k");
    written_bytes += write_member(m_n_solid_nodes, out, child, "solid_kmers");
    written_bytes += write_member(m_m, out, child, "m");

    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

void vo_boss::load(std::istream& in){
    m_lcs.load(in);
    m_edge_bwt.load(in);
    m_node_marks.load(in);
    m_node_marks_rs.load(in, &m_node_marks);
    m_node_marks_ss.load(in, &m_node_marks);
    m_solid_nodes.load(in);
    m_solid_nodes_ss.load(in, &m_solid_nodes);
    read_member(m_k, in);
    read_member(m_n_solid_nodes, in);
    read_member(m_m, in);
}

vo_boss::vo_boss(): node_marks(m_node_marks),
                        edge_bwt(m_edge_bwt),
                        node_marks_rs(m_node_marks_rs){}


void vo_boss::build_klcp(cache_config &config, size_t K)  {

    LOG(INFO)<<"Building the LCP of the kmers";

    size_t n_dollars;
    int_vector_buffer<> lcp(cache_file_name(conf::KEY_LCP, config));
    int_vector_buffer<> klcp(cache_file_name("klcp", config), std::ios::out, 1000000);
    int_vector_buffer<> klcp_final(cache_file_name("klcp_tmp", config), std::ios::out, 1000000);

    int_vector_buffer<> sa(cache_file_name(conf::KEY_SA, config));
    sdsl::sd_vector<> dollar_bv;
    load_from_file(dollar_bv, cache_file_name("sp_dollar_bv", config));
    sdsl::sd_vector<>::rank_1_type dr(&dollar_bv);
    sdsl::sd_vector<>::select_1_type ds(&dollar_bv);
    n_dollars = dr.rank(dollar_bv.size());

    klcp[0] = 0;
    //dummy node
    for(size_t i=1;i<=n_dollars;i++){
        klcp[i] = K;
    }

    size_type klcp_final_pos=1;
    klcp_final[0] = 0;
    for(size_t i=(n_dollars+1);i<lcp.size();i++){
        //modify the LCP
        if(lcp[i]>K || lcp[i]>=(ds.select(dr.rank(sa[i]+1)+1)-sa[i]+1)){
            klcp[i]=K;
        }else{
            klcp[i] = lcp[i];
        }

        if(klcp[i]<K){
            klcp_final[klcp_final_pos] = klcp[i];
            klcp_final_pos++;
        }
    }

    lcp.close();
    klcp.close();
    klcp_final.close();
    t_lcs wt_lcs;
    construct(wt_lcs, cache_file_name("klcp_tmp", config));
    register_cache_file("klcp_tmp", config);
    register_cache_file("klcp", config);
    m_lcs.swap(wt_lcs);
}

void vo_boss::build_edgebwt(cache_config &config, size_t K) {

    LOG(INFO)<<"Building the edgeBWT";
    bool flag_symbols;
    std::bitset<6> kmer_symbols;
    std::bitset<6> flagged_symbols=false;
    bit_vector in_marks, node_edges_marks;
    int_vector_buffer<8> bwt(cache_file_name(conf::KEY_BWT, config));
    int_vector_buffer<8> ebwt(cache_file_name("ebwt", config), std::ios::out);
    int_vector_buffer<> klcp(cache_file_name("klcp", config));
    size_t bwt_pos=0;

    //this is for marking solid nodes
    sdsl::sd_vector<> dollar_bv;
    bit_vector dollar_marks;
    load_from_file(dollar_bv, cache_file_name("sp_dollar_bv", config));
    sdsl::sd_vector<>::rank_1_type dr(&dollar_bv);
    int_vector_buffer<> sa(cache_file_name(conf::KEY_SA, config));
    util::assign(dollar_marks, int_vector<1>(bwt.size(), 0));
    size_t dpos=0;
    //

    util::assign(node_edges_marks, int_vector<1>(bwt.size(), 1));
    util::assign(in_marks , int_vector<1>(bwt.size(), 0));

    for(size_t i=1;i<=klcp.size();i++){

        if(i==klcp.size() || klcp[i]<K){

            if(dr.rank(min(sa[i-1]+K, sa.size()))-dr.rank(sa[i-1])==0){
                dollar_marks[dpos]=true;
                m_n_solid_nodes++;
            }

            //store the symbols of the previous kmer
            for(size_t j=1;j<kmer_symbols.size();j++){
                if(kmer_symbols[j]){

                    if(flagged_symbols[j] && j>1) {// edge is marked (except $ symbols)
                        in_marks[bwt_pos] = true;
                    }

                    ebwt[bwt_pos]=j;
                    bwt_pos++;
                }
            }

            node_edges_marks[bwt_pos-1] = false;
            dpos++;

            if(i==klcp.size()){
               break;
            }

            //setup for the next Kmer;
            flag_symbols = klcp[i] == (K - 1);
            if (!flag_symbols) {
                flagged_symbols.reset();
            } else {
                flagged_symbols |= kmer_symbols;
            }
            kmer_symbols.reset();
        }
        kmer_symbols.set(bwt[i]);
    }

    node_edges_marks.resize(bwt_pos);
    in_marks.resize(bwt_pos);

    ebwt.close();
    klcp.close();

    //this is for marking solid nodes
    dollar_marks.resize(dpos);
    sdsl::rrr_vector<63> rrr_dollar_marks(dollar_marks);
    store_to_file(rrr_dollar_marks, cache_file_name("solid_nodes", config));
    //

    sdsl::rrr_vector<63> tmp_node_marks(node_edges_marks);
    store_to_file(tmp_node_marks, cache_file_name("node_edges_marks", config));
    store_to_file(in_marks, cache_file_name("in_marks", config));

    register_cache_file("solid_nodes", config);
    register_cache_file("node_edges_marks", config);
    register_cache_file("in_marks", config);
    register_cache_file("ebwt", config);
}

std::vector<std::pair<size_t, size_t>> vo_boss::backward_search(vector<uint8_t> &query, uint8_t mismatches) {

    std::stack<std::tuple<size_t, size_t, uint8_t, size_t>> ranges;
    std::vector<std::pair<size_t, size_t>> res;

    std::vector<uint8_t> symbols(7,0);
    std::vector<uint64_t> rank_i(7,0);
    std::vector<uint64_t> rank_j(7,0);
    uint8_t mm;
    size_t e_start, e_end, k_start, k_end, last_index, n_symbols;

    std::tuple<size_t, size_t, uint8_t, size_t> root(0,m_edge_bwt.m_runs_acc[5], 0, query.size());
    ranges.push(root);

    while(!ranges.empty()){
        std::tuple<size_t, size_t, uint8_t, uint8_t> tmp = ranges.top();
        ranges.pop();
        last_index = std::get<3>(tmp);
        if(last_index>0) {

            if(std::get<0>(tmp)==0){
                e_start=0;
            }else {
                e_start = m_node_marks_ss.select(std::get<0>(tmp)) + 1;
            }
            e_end=m_node_marks_ss.select(std::get<1>(tmp)+1);

            n_symbols = m_edge_bwt.range_symbols(e_start,
                                                 e_end,
                                                 symbols, rank_i, rank_j);
            for(size_t i=0;i<n_symbols;i++){
                mm = std::get<2>(tmp) + (symbols[i] != query[last_index - 1]);
                if (mm <= mismatches) {

                    k_start = m_edge_bwt.runs_acc[symbols[i]-1] + rank_i[i];
                    k_end = m_edge_bwt.runs_acc[symbols[i]-1] + rank_j[i];

                    std::tuple<size_t, size_t, uint8_t, size_t> next_range(k_start, k_end, mm, last_index-1);
                    ranges.push(next_range);
                }
            }
        }else{
            res.emplace_back(std::get<0>(tmp),std::get<1>(tmp));
        }
    }

    if(res.empty()) res.emplace_back(0,0);

    return res;
}

std::pair<vo_boss::size_type, bool> vo_boss::dbg_indegree(vo_boss::size_type v) const {
    uint8_t symbol = m_edge_bwt.pos2symbol(v);
    size_t rank = v - m_edge_bwt.m_runs_acc[symbol-1];
    size_t start, end;
    bool has_dollars;

    if(v==0) return make_pair(0,true);

    start = m_edge_bwt.select(rank, symbol);

    //this fits better in the select operation
    if(rank+1<=(m_edge_bwt.m_runs_acc[symbol] - m_edge_bwt.m_runs_acc[symbol - 1])){
        end = m_edge_bwt.select(rank+1, symbol);
    }else{
        end = m_edge_bwt.size();
    }

    size_t dollars_s = m_edge_bwt.m_dollars_rs.rank(start);
    size_t dollars_e = m_edge_bwt.m_dollars_rs.rank(end);

    size_t inv_s = m_edge_bwt.m_in_marks_rs.rank(start-dollars_s);
    size_t inv_e = m_edge_bwt.m_in_marks_rs.rank(end-dollars_e);

    has_dollars = m_solid_nodes[m_node_marks_rs.rank(start)]==0;

    return std::make_pair(1+m_edge_bwt.m_f_symbols.rank(inv_e, symbol)-m_edge_bwt.m_f_symbols.rank(inv_s, symbol),
                          has_dollars);
}

vo_boss::size_type vo_boss::reverse_complement(vo_boss::size_type v) {
    std::vector<uint8_t> kmer = node_label(v);
    std::vector<uint8_t> reverse_complement(m_k-1);
    if(v==0) return 0;

    for(size_t i=0,j=kmer.size()-1;i<kmer.size();i++,j--){
        reverse_complement[i] = dna_alphabet::comp2rev[kmer[j]];
    }
    //backward search return the range in the bwt not the kmer id!!
    std::vector<std::pair<size_t, size_t>> ranges = backward_search(reverse_complement, 0);
    if(ranges.empty()){
        return 0;
    }else {
        return ranges[0].first;
    }
}

vo_boss::node_t vo_boss::next_contained(vo_boss::node_t node) {

    size_t order = node.order-1;
    while(order>=m_m) {
        node_t tmp_node = shorter(node,order);
        if(tmp_node.start<node.start){
            if(!solid_nodes[tmp_node.start] &&
               node_llabel(tmp_node.start).size()==order){
                return tmp_node;
            }
            node = tmp_node;
        }
        order--;
    }
    return {0,num_of_nodes()-1,0};
}

std::vector<uint8_t> vo_boss::node_llabel(vo_boss::size_type v) const {

    uint8_t symbol;
    assert(!m_solid_nodes[v]);
    std::vector<uint8_t> llabel;

    while(v!=0){
        symbol = m_edge_bwt.pos2symbol(v);
        llabel.push_back(symbol);
        size_t rank = v - m_edge_bwt.m_runs_acc[symbol-1];
        v = node_marks_rs.rank(m_edge_bwt.select(rank, symbol));
    }
    return llabel;
}

vector<vo_boss::size_type> vo_boss::buildL(vo_boss::node_t node) {
    std::vector<size_type> l;
    node = next_contained(node);
    while(node != root()){
        l.push_back(node.start);
        node = next_contained(node);
    }
    return l;
}

vector<vo_boss::size_type> vo_boss::foverlaps(node_t node) {

    assert(m_solid_nodes[node.start]);

    std::vector<size_type> f_overlaps;
    node_t nc = next_contained(node);
    if(nc==root()) return f_overlaps;

    std::vector<uint8_t> m_suffix(m_m);
    std::vector<uint8_t> greatest_linker = node_llabel(nc.start);

    //compute the reverse complement of the m_suffix
    size_t suff_pos = m_m-1;
    for (size_t i = 0; i <m_m;i++){
        m_suffix[suff_pos] = dna_alphabet::comp2rev[greatest_linker[i]];
        suff_pos--;
    }

    //backward search of the reverse complement of the m-suffix
    std::vector<std::pair<size_t, size_t>> bs = backward_search(m_suffix, 0);

    std::vector<uint8_t> symbols(7,0);
    std::vector<uint64_t> rank_i(7,0);
    std::vector<uint64_t> rank_j(7,0);
    size_t e_start, e_end, k_start, k_end, n_symbols;

    k_start = bs[0].first;
    k_end = bs[0].second;

    for (size_t i = m_m; i <greatest_linker.size(); i++) {

        for(size_t j=k_start;j<=k_end;j++){
            if(has_dollar_edges(get_edges(j))){
                //std::cout<<node2string(reverse_complement(j), true)<<std::endl;
            }
        }

        e_start = m_node_marks_ss.select(k_start) + 1;
        e_end= m_node_marks_ss.select(k_end+1);

        n_symbols = m_edge_bwt.range_symbols(e_start,
                                             e_end,
                                             symbols, rank_i, rank_j);
        bool found=false;

        for (size_t u = 0; u < n_symbols; u++) {
            if(symbols[u]==dna_alphabet::comp2rev[greatest_linker[i]]){
                found = true;
                k_start = m_edge_bwt.runs_acc[symbols[u] - 1] + rank_i[u];
                k_end = m_edge_bwt.runs_acc[symbols[u] - 1] + rank_j[u];

            }
        }

        if(!found) break;

    }

    //counting for the last one
    for(size_t j=k_start;j<=k_end;j++){
        if(has_dollar_edges(get_edges(j))){
            //std::cout<<node2string(reverse_complement(j), true)<<std::endl;
        }
    }

    return f_overlaps;
}
