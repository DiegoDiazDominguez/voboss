//
// Created by diediaz on 10-09-18.
//

#include <rl_edge_bwt.hpp>
#include "rl_edge_bwt.hpp"

using namespace sdsl;

rl_edge_bwt::rl_edge_bwt(cache_config &config): tmp_symbols(7,0), tmp_rank_i(7,0), tmp_rank_j(7,0){

    LOG(INFO)<<"Storing the edgeBWT using RL encoding";
    size_t rlbwt_pos=0, bwt_pos=0, ebwt_flagged_pos=0, run_length=0,  ebwt_heads_pos=0,  curr_head;
    size_t runs_sizes[6]={0};
    uint64_t symbol;

    int_vector_buffer<8> ebwt(cache_file_name("ebwt", config));
    sdsl::bit_vector in_marks, dollars, ebwt_runs, ebwt_runs_sums, in_marks_rl;
    load_from_file(in_marks, cache_file_name("in_marks", config));

    int_vector_buffer<8> ebwt_heads(cache_file_name("ebwt_heads", config), std::ios::out, 1000000);
    int_vector_buffer<8> ebwt_fg(cache_file_name("ebwt_fg", config), std::ios::out, 1000000);

    util::assign(dollars, int_vector<1>(ebwt.size(), 0));
    util::assign(in_marks_rl, int_vector<1>(ebwt.size(), 0));
    util::assign(ebwt_runs, int_vector<1>(ebwt.size(), 0));
    util::assign(ebwt_runs_sums, int_vector<1>(ebwt.size(), 0));

    bwt_size = ebwt.size();

    //initialize the first head
    size_t pos=0;
    while(ebwt[pos]==1){//reach the first non $ symbol
        pos++;
    }

    ebwt_heads[ebwt_heads_pos] = ebwt[pos];
    run_length=1;
    ebwt_runs[rlbwt_pos] = true;
    rlbwt_pos=1;
    bwt_pos=1;

    for(size_t i=pos+1;i<ebwt.size();i++){
        symbol = ebwt[i];
        if(symbol==1){//$ edge
            dollars[i]=true;
        }else {

            if(in_marks[i]) {// duplicated dbg_incomming edge
                ebwt_fg[ebwt_flagged_pos] = symbol;
                in_marks_rl[bwt_pos] = true;
                ebwt_flagged_pos++;
            } else {
                if(ebwt_heads[ebwt_heads_pos] != symbol) { //new run head
                    runs_sizes[ebwt_heads[ebwt_heads_pos]] += run_length; //size of the recently finished run
                    run_length = 0;
                    ebwt_runs[rlbwt_pos] = true;
                    ebwt_heads_pos++;
                    ebwt_heads[ebwt_heads_pos] = symbol;
                }
                run_length++;
                rlbwt_pos++;
            }
            bwt_pos++;
        }
    }

    //count the size of the last run
    runs_sizes[ebwt_heads[ebwt_heads_pos]] += run_length;

    dollars.resize(ebwt.size());
    in_marks_rl.resize(bwt_pos);
    ebwt_runs.resize(rlbwt_pos);
    ebwt_runs_sums.resize(rlbwt_pos);

    //build the sums bit vector
    for(size_t i=1;i<6;i++){
        m_runs_acc[i] = runs_sizes[i-1] + m_runs_acc[i-1];
    }

    ebwt_heads_pos=1;
    curr_head = ebwt_heads[0];
    run_length=1;
    for(size_t i=1;i<ebwt_runs.size();i++){
        if(ebwt_runs[i]){
            ebwt_runs_sums[m_runs_acc[curr_head]] = true;
            //std::cout<<curr_head<<" "<<runs_acc[curr_head]<<" "<<run_length<<std::endl;
            m_runs_acc[curr_head] += run_length;
            curr_head = ebwt_heads[ebwt_heads_pos++];
            run_length=0;
        }
        run_length++;
    }
    ebwt_runs_sums[m_runs_acc[curr_head]] = true;
    m_runs_acc[curr_head] += run_length;

    //TODO testing
    /*for(size_t i=0;i<70;i++){
        std::cout<<i<<" "<<dna_alphabet::comp2char[ebwt[i]]<<" "<<in_marks[i]<<" "<<dollars[i]<<std::endl;
    }
    std::cout<<""<<std::endl;
    pos=0;
    for(size_t i=0;i<ebwt.size();i++){
        if(ebwt[i]!=1 && !in_marks[i]) {
            std::cout<<pos<<" "<<dna_alphabet::comp2char[ebwt[i]]<<" ";
            std::cout<<ebwt_runs[pos]<<std::endl;
            pos++;
        }
    }
    std::cout<<""<<std::endl;*/
    //TODO end testing

    ebwt.close();
    ebwt_heads.close();
    ebwt_fg.close();

    register_cache_file("ebwt_heads", config);
    register_cache_file("ebwt_fg", config);


    sdsl::construct(m_h_symbols, cache_file_name("ebwt_heads", config));
    sdsl::construct(m_f_symbols, cache_file_name("ebwt_fg", config));

    //TODO just testing
    /*sdsl::sd_vector<> tmp2_dollars(dollars);
    sdsl::sd_vector<> tmp2_in_marks(in_marks_rl);
    sdsl::sd_vector<> tmp2_runs(ebwt_runs);
    sdsl::sd_vector<> tmp2_runs_sums(ebwt_runs_sums);
     */
    //

    sdsl::rrr_vector<63> tmp_dollars(dollars);
    sdsl::rrr_vector<63> tmp_in_marks(in_marks_rl);
    sdsl::rrr_vector<63> tmp_runs(ebwt_runs);
    sdsl::rrr_vector<63> tmp_runs_sums(ebwt_runs_sums);

    //TODO testing
    /*
    std::cout<<sdsl::size_in_mega_bytes(tmp2_dollars)<<std::endl;
    std::cout<<sdsl::size_in_mega_bytes(tmp_dollars)<<std::endl;
    std::cout<<"marks"<<std::endl;
    std::cout<<sdsl::size_in_mega_bytes(tmp2_in_marks)<<std::endl;
    std::cout<<sdsl::size_in_mega_bytes(tmp_in_marks)<<std::endl;
    std::cout<<"runs"<<std::endl;
    std::cout<<sdsl::size_in_mega_bytes(tmp2_runs)<<std::endl;
    std::cout<<sdsl::size_in_mega_bytes(tmp2_runs_sums)<<std::endl;
     */
    //

    m_dollars.swap(tmp_dollars);
    m_in_marks.swap(tmp_in_marks);
    m_symbol_runs.swap(tmp_runs);
    m_symbol_runs_sums.swap(tmp_runs_sums);

    m_dollars_rs.set_vector(&m_dollars);
    m_dollars_ss.set_vector(&m_dollars);
    m_in_marks_rs.set_vector(&m_in_marks);
    m_in_marks_ss.set_vector(&m_in_marks);

    m_symbol_runs_rs.set_vector(&m_symbol_runs);
    m_symbol_runs_ss.set_vector(&m_symbol_runs);
    m_symbol_runs_sums_rs.set_vector(&m_symbol_runs_sums);
    m_symbol_runs_sums_ss.set_vector(&m_symbol_runs_sums);

    m_sigma = (uint8_t)m_h_symbols.sigma;

    //build the C array for the heads
    size_t prev=0, tmp=0;
    for(uint8_t i=1;i<6;i++){
        tmp = m_h_symbols.rank(m_h_symbols.size(), i);
        m_C[i] = m_C[i-1] + prev;
        prev = tmp;
    }
    m_C[6] = m_C[5] + tmp;
}

uint8_t rl_edge_bwt::operator[](size_t index)const{
    assert(index<bwt_size);
    size_t n_dolls, n_marks, rl_index, n_runs, n_in_marked;
    if(m_dollars[index]){//position is $
        return 1;
    }else{
        n_dolls = m_dollars_rs.rank(index + 1);
        if(m_in_marks[index-n_dolls]){//position is marked
            n_in_marked = m_in_marks_rs.rank(index-n_dolls+1);
            return m_f_symbols[n_in_marked-1];
        } else {//position is in the RLBWT
            n_marks = m_in_marks_rs.rank(index-n_dolls + 1);
            rl_index = index - n_dolls - n_marks;
            n_runs = m_symbol_runs_rs(rl_index + 1);
            return m_h_symbols[n_runs - 1];
        }
    }
}

//here we assume that index is a position in the BWT with BWT[index] = symbol
rl_edge_bwt::size_type rl_edge_bwt::LFmapping(size_t index)const{
    //TODO check this
    size_t rl_index, n_dolls, n_marks, n_runs, head_rank, delta, prev_run;
    uint8_t head_symbol;
    size_t is_marked;

    if(m_dollars[index]){
        return 0;
    }

    n_dolls = m_dollars_rs.rank(index+1);
    is_marked = m_in_marks[index-n_dolls];

    //get the index relative to the RLBWT
    n_marks = m_in_marks_rs.rank(index-n_dolls+1);
    rl_index = index - n_dolls - n_marks;

    n_runs = m_symbol_runs_rs.rank(rl_index+1);
    if(is_marked==1){
        head_symbol = m_f_symbols[n_marks-1];
    }else{
        head_symbol = m_h_symbols[n_runs-1];
    }

    head_rank = m_h_symbols.rank(n_runs, head_symbol);
    prev_run = m_C[head_symbol] + head_rank;

    //std::cout<<m_symbol_runs_ss(n_runs)<<std::endl;

    if(is_marked==1){
        size_t run_rank = m_h_symbols.select(head_rank, head_symbol)+1;
        size_t end_run = m_symbol_runs_ss.select(run_rank+1)-1;
        delta = std::min((index-n_dolls-n_marks),end_run) - m_symbol_runs_ss.select(run_rank);
    }else{
        //TODO check this delta
        delta=  (index-n_dolls-n_marks)-m_symbol_runs_ss.select(n_runs);
    }
    return m_symbol_runs_sums_ss.select(prev_run) + 1 + delta; //the 1 is for the dummy kmer
}

//returns the symbols in a range
rl_edge_bwt::size_type rl_edge_bwt::range_symbols(size_t start_index, size_t end_index, std::vector<uint8_t>& symbols,
                              std::vector<uint64_t>& rank_i, std::vector<uint64_t>& rank_j) {

    size_t n_dollars_s, n_dollars_e, inv_s, inv_e, rl_s, rl_e, run_s, run_e, n_symbols=0, tmp;
    uint64_t k;
    bool m_symbols[6]={false};

    //number of $ in the range
    n_dollars_s = m_dollars_rs.rank(start_index);
    n_dollars_e = m_dollars_rs.rank(end_index+1);
    if((n_dollars_e-n_dollars_s)==(end_index-start_index+1)){//dbg_outdegree 0
        return 0;
    }

    //number of marked symbols in the range
    inv_s = m_in_marks_rs.rank(start_index-n_dollars_s);
    inv_e = m_in_marks_rs.rank(end_index-n_dollars_e+1);

    rl_s = start_index - n_dollars_s - inv_s;
    rl_e = end_index - n_dollars_e - inv_e;

    if(rl_s>rl_e){
        run_s = m_symbol_runs_rs.rank(rl_e+1);
    }else{
        run_s = m_symbol_runs_rs.rank(rl_s+1);
    }

    if(rl_s<=rl_e){//there are unmarked symbols
        run_e = m_symbol_runs_rs.rank(rl_e+1);
        m_h_symbols.interval_symbols(run_s-1, run_e, k, symbols, rank_i, rank_j);
        n_symbols=k;
        for(size_t i=0;i<n_symbols;i++){
            m_symbols[symbols[i]]=true;
            tmp = m_symbol_runs_sums_ss.select(m_C[symbols[i]]+ rank_i[i]+1) + 1 - m_runs_acc[symbols[i]-1];

            size_t head_rank = m_h_symbols.select(rank_i[i]+1,symbols[i])+1;
            size_t head_start = m_symbol_runs_ss.select(head_rank);
            rank_i[i] = tmp;
            if(head_start<rl_s){
                rank_i[i] += rl_s - head_start;
            }

            tmp = m_symbol_runs_sums_ss.select(m_C[symbols[i]]+ rank_j[i]) + 1 - m_runs_acc[symbols[i]-1];
            head_rank = m_h_symbols.select(rank_j[i],symbols[i])+1;
            head_start = m_symbol_runs_ss.select(head_rank);
            size_t head_end = m_symbol_runs_ss.select(head_rank+1)-1;
            rank_j[i] = tmp + (std::min<size_t>(head_end, rl_e) - head_start);
        }
    }

    if(rl_s>rl_e)rl_s=rl_e;
    if((inv_e-inv_s)>0){//there are marked symbols
        m_f_symbols.interval_symbols(inv_s, inv_e, k, tmp_symbols, tmp_rank_i, tmp_rank_j);
        for(size_t i=0;i<k;i++){
            if(!m_symbols[tmp_symbols[i]]){
                size_t head_rank = m_h_symbols.rank(run_s, tmp_symbols[i]);
                size_t head_acc = m_symbol_runs_sums_ss.select(m_C[tmp_symbols[i]]+head_rank)- m_runs_acc[tmp_symbols[i]-1]+1;
                size_t head_start = m_symbol_runs_ss.select(m_h_symbols.select(head_rank, tmp_symbols[i])+1);
                size_t head_end = m_symbol_runs_ss.select(m_h_symbols.select(head_rank, tmp_symbols[i])+2)-1;
                head_acc+=std::min(rl_s, head_end)-head_start;
                symbols[n_symbols] = tmp_symbols[i];
                rank_i[n_symbols] = head_acc;
                rank_j[n_symbols] = rank_i[n_symbols];
                n_symbols++;
            }
        }
    }
    return n_symbols;
}

rl_edge_bwt::size_type rl_edge_bwt::size() const {
    return bwt_size;
}

rl_edge_bwt::rl_edge_bwt():tmp_symbols(7,0), tmp_rank_i(7,0), tmp_rank_j(7,0){};

void rl_edge_bwt::load(std::istream &in) {

    m_h_symbols.load(in);
    m_f_symbols.load(in);
    m_symbol_runs.load(in);
    m_symbol_runs_sums.load(in);
    m_dollars.load(in);
    m_in_marks.load(in);

    m_dollars_rs.load(in, &m_dollars);
    m_in_marks_rs.load(in, &m_in_marks);
    m_dollars_ss.load(in, &m_dollars);
    m_in_marks_ss.load(in, &m_in_marks);
    m_symbol_runs_rs.load(in, &m_symbol_runs);
    m_symbol_runs_ss.load(in, &m_symbol_runs);
    m_symbol_runs_sums_rs.load(in, &m_symbol_runs_sums);
    m_symbol_runs_sums_ss.load(in, &m_symbol_runs_sums);

    read_member(bwt_size, in);
    read_member(m_sigma, in);
    m_runs_acc.load(in);
    m_C.load(in);

    tmp_symbols = std::vector<uint8_t>(7,0);
    tmp_rank_i = std::vector<uint64_t>(7,0);
    tmp_rank_j = std::vector<uint64_t>(7,0);
}

rl_edge_bwt::size_type rl_edge_bwt::serialize(std::ostream &out, structure_tree_node *v, std::string name) const {
    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_t written_bytes = 0;
    written_bytes += m_h_symbols.serialize(out, child, "m_h_symbols");
    written_bytes += m_f_symbols.serialize(out, child, "m_f_symbols");
    written_bytes += m_symbol_runs.serialize(out, child, "m_symbol_runs");
    written_bytes += m_symbol_runs_sums.serialize(out, child, "m_symbol_runs_sums");
    written_bytes += m_dollars.serialize(out, child, "m_dollars");
    written_bytes += m_in_marks.serialize(out, child, "m_in_marks");

    written_bytes += m_dollars_rs.serialize(out, child, "m_dollars_rs");
    written_bytes += m_in_marks_rs.serialize(out, child, "m_in_marks_rs");
    written_bytes += m_dollars_ss.serialize(out, child, "m_dollars_ss");
    written_bytes += m_in_marks_ss.serialize(out, child, "m_in_marks_ss");
    written_bytes += m_symbol_runs_rs.serialize(out, child, "m_symbol_runs_rs");
    written_bytes += m_symbol_runs_ss.serialize(out, child, "m_symbol_runs_ss");
    written_bytes += m_symbol_runs_sums_rs.serialize(out, child, "m_symbol_runs_sums_rs");
    written_bytes += m_symbol_runs_sums_ss.serialize(out, child, "m_symbol_runs_sums_ss");

    written_bytes += write_member(bwt_size, out, child, "bwt_size");
    written_bytes += write_member(m_sigma, out, child, "m_sigma");
    written_bytes += m_runs_acc.serialize(out, child, "m_runs_acc");
    written_bytes += m_C.serialize(out, child, "m_C");

    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}


void rl_edge_bwt::copy(const rl_edge_bwt &other) {

}

/*size_t rl_edge_bwt::edge_rank(size_t index, uint8_t symbol) {
    assert(index<bwt_size && symbol>=1);
    size_t n_dollars, n_marked, rl_index, n_runs, run_rank, n_symbols=0;
    size_t n_symbol_heads, last_run_size, delta=0, last_head_rank, lh_s, lh_e;

    n_dollars = m_dollars_rs.rank(index+1);
    if(symbol==1) return n_dollars;
    n_marked = m_in_marks_rs.rank(index+1);

    n_symbols+= m_f_symbols.rank(n_marked, symbol);

    rl_index = index - n_marked-n_dollars;
    n_runs = m_symbol_runs_rs.rank(rl_index+1);

    n_symbol_heads = m_h_symbols.rank(n_runs, symbol);
    if(n_symbol_heads==0) return n_symbols;

    run_rank = m_C[symbol] + n_symbol_heads;

    //number of the symbols previous the last run before index
    n_symbols += m_symbol_runs_sums_ss.select(run_rank) - m_runs_acc[symbol-1];

    //the number of symbols in the last run
    last_head_rank = m_h_symbols.select(n_symbol_heads,symbol)+1;
    lh_s = m_symbol_runs_ss.select(last_head_rank);
    lh_e = m_symbol_runs_ss.select(last_head_rank+1)-1;
    last_run_size = lh_e - lh_s+1;

    if(lh_e>rl_index)delta=lh_e-rl_index;

    n_symbols +=last_run_size-delta;
    return n_symbols;
}*/

rl_edge_bwt::size_type rl_edge_bwt::select(size_t rank, uint8_t symbol) const {
    if(rank==0) return 0;
    assert(symbol>1 && symbol<=5 && rank<= m_runs_acc[symbol]-m_runs_acc[symbol-1]);
    size_t n_runs, delta, run_pos, pos;
    pos = m_runs_acc[symbol-1]+rank;
    n_runs = m_symbol_runs_sums_rs.rank(pos)-m_C[symbol];
    run_pos = m_symbol_runs_sums_ss.select(m_C[symbol]+n_runs);
    delta = pos-run_pos-1;
    return m_dollars_ss.select(
            m_in_marks_ss.select(
                    m_symbol_runs_ss.select(
                            m_h_symbols.select(n_runs, symbol)+1)+delta+1)+1);
}

//return true if there is one symbol in the range
bool rl_edge_bwt::has_one_symbol(size_t start_index, size_t end_index, uint8_t &symbol, size_t &rank_i, size_t &rank_j) {

    size_t range_size, n_dollars_s, n_dollars_e, n_dollars, inv_s, inv_e, n_inv, rl_s, rl_e, run_s, run_e;

    range_size = end_index-start_index+1;

    //number of $ in the range
    n_dollars_s = m_dollars_rs.rank(start_index);
    n_dollars_e = m_dollars_rs.rank(end_index+1);
    n_dollars = n_dollars_e - n_dollars_s;

    if(n_dollars==range_size){//dbg_outdegree 0
        symbol=0;
        rank_i=0;
        rank_j=0;
        return false;
    }

    //number of marked symbols in the range
    inv_s = m_in_marks_rs.rank(start_index-n_dollars_s);
    inv_e = m_in_marks_rs.rank(end_index-n_dollars_e+1);
    n_inv = inv_e-inv_s;

    rl_s = start_index - n_dollars_s - inv_s;
    rl_e = end_index - n_dollars_e - inv_e;

    if(rl_s>rl_e){
        run_s = m_symbol_runs_rs.rank(rl_s);
    }else{
        run_s = m_symbol_runs_rs.rank(rl_s+1);
    }

    if((n_inv+n_dollars)!=range_size){//there are unmarked symbols
        run_e = m_symbol_runs_rs.rank(rl_e+1);

        symbol = m_h_symbols[run_s-1];
        rank_i = m_h_symbols.rank(run_s, symbol);
        rank_j = m_h_symbols.rank(run_e, symbol);

        if((rank_j-rank_i+1)!=(run_e-run_s+1)){
            symbol=0;
            rank_i=0;
            rank_j=0;
            return false;
        };
    }

    if(n_inv>0){//there are marked symbols
        size_t tmp_rank_i, tmp_rank_j;
        uint8_t tmp_symbol;

        tmp_symbol = m_f_symbols[inv_s];
        if(symbol!=0 && tmp_symbol!=symbol){
            symbol=0;
            rank_i=0;
            rank_j=0;
            return false;
        }

        tmp_rank_i = m_f_symbols.rank(inv_s+1, tmp_symbol);
        tmp_rank_j = m_f_symbols.rank(inv_e, tmp_symbol);
        if((tmp_rank_j-tmp_rank_i+1)!=(n_inv)){
            symbol=0;
            rank_i=0;
            rank_j=0;
            return false;
        }

        if(symbol==0){
            symbol = tmp_symbol;
            rank_i = m_h_symbols.rank(run_s, symbol);
            rank_j = rank_i;
        }
    }
    return true;
}

void rl_edge_bwt::swap(rl_edge_bwt& other) {
    m_sigma = other.m_sigma;
    bwt_size = other.bwt_size;

    m_h_symbols.swap(other.m_h_symbols);
    m_f_symbols.swap(other.m_f_symbols);
    m_symbol_runs.swap(other.m_symbol_runs);
    m_symbol_runs_sums.swap(other.m_symbol_runs_sums);
    m_dollars.swap(other.m_dollars);
    m_in_marks.swap(other.m_in_marks);

    m_dollars_rs.swap(other.m_dollars_rs);
    m_dollars_rs.set_vector(&m_dollars);
    m_dollars_ss.swap(other.m_dollars_ss);
    m_dollars_ss.set_vector(&m_dollars);
    m_in_marks_rs.swap(other.m_in_marks_rs);
    m_in_marks_rs.set_vector(&m_in_marks);
    m_in_marks_ss.swap(other.m_in_marks_ss);
    m_in_marks_ss.set_vector(&m_in_marks);
    m_symbol_runs_rs.swap(other.m_symbol_runs_rs);
    m_symbol_runs_rs.set_vector(&m_symbol_runs);
    m_symbol_runs_ss.swap(other.m_symbol_runs_ss);
    m_symbol_runs_ss.set_vector(&m_symbol_runs);
    m_symbol_runs_sums_ss.swap(other.m_symbol_runs_sums_ss);
    m_symbol_runs_sums_ss.set_vector(&m_symbol_runs_sums);
    m_symbol_runs_sums_rs.swap(other.m_symbol_runs_sums_rs);
    m_symbol_runs_sums_rs.set_vector(&m_symbol_runs_sums);

    m_C.swap(other.m_C);
    m_runs_acc.swap(other.m_runs_acc);
}

uint8_t rl_edge_bwt::pos2symbol(rl_edge_bwt::size_type kmer_pos) const {
    uint8_t symbol=0;
    assert(kmer_pos<=runs_acc[5]);
    if(kmer_pos==0) return 1; //dummy

    for(uint8_t i=2;i<6;i++){
        if(runs_acc[i-1]<kmer_pos && runs_acc[i]>=kmer_pos){
            symbol = i;
        }
    }
    return symbol;
}

bool rl_edge_bwt::same_symbol(const std::vector<size_type> &positions) {

    assert(!positions.empty());
    size_t tmp, pos;
    std::vector<size_type> marked_positions;
    std::vector<size_type> runs_positions;

    std::bitset<8> symbols;
    std::vector<uint8_t> final_symbols;

    //TODO break earlier if all are in the same symbol

    for (auto &position : positions) {

        //remove dollars
        tmp = m_dollars_rs.rank(position);
        pos = position - tmp;

        //marked symbols
        if(m_in_marks[pos]){
            marked_positions.push_back(m_in_marks_rs.rank(pos));
        }else { //RL symbols
            tmp = m_in_marks_rs.rank(pos);
            pos = pos - tmp;

            //get range of run heads
            tmp = m_symbol_runs_rs.rank(pos);
            if (!m_symbol_runs[pos]) tmp--;
            pos = tmp;
            runs_positions.push_back(pos);
        }
    }

    for (auto &mp : marked_positions ) {
        symbols[m_f_symbols[mp]]=true;
        if(symbols.count()>1){
           return false;
        }
    }

    for (auto &mp : runs_positions ) {
        symbols[m_h_symbols[mp]]=true;
        if(symbols.count()>1){
            return false;
        }
    }
    return true;
}

void rl_edge_bwt::print_dt_sizes() {
    double tot_size = sdsl::size_in_mega_bytes(*this);
    std::cout<<"tot size: "<<tot_size<<std::endl;
    std::cout<<double(m_dollars_rs(m_dollars.size()))/m_dollars.size()<<std::endl;
    std::cout<<"run_heads: "<<(sdsl::size_in_mega_bytes(m_h_symbols)/tot_size)*100<<std::endl;
    std::cout<<"marked_symbols: "<<(sdsl::size_in_mega_bytes(m_f_symbols)/tot_size)*100<<std::endl;
    std::cout<<"symbol_marks: "<<(sdsl::size_in_mega_bytes(m_in_marks)/tot_size)*100<<std::endl;
    std::cout<<"symbol_marks rs: "<<(sdsl::size_in_mega_bytes(m_in_marks_rs)/tot_size)*100<<std::endl;
    std::cout<<"symbol_marks ss: "<<(sdsl::size_in_mega_bytes(m_in_marks_ss)/tot_size)*100<<std::endl;
    std::cout<<"dollar_marks: "<<(sdsl::size_in_mega_bytes(m_dollars)/tot_size)*100<<std::endl;
    std::cout<<"dollar_marks rs: "<<(sdsl::size_in_mega_bytes(m_dollars_rs)/tot_size)*100<<std::endl;
    std::cout<<"dollar_marks ss: "<<(sdsl::size_in_mega_bytes(m_dollars_ss)/tot_size)*100<<std::endl;
    std::cout<<"symbol runs: "<<(sdsl::size_in_mega_bytes(m_symbol_runs)/tot_size)*100<<std::endl;
    std::cout<<"symbol runs rs: "<<(sdsl::size_in_mega_bytes(m_symbol_runs_rs)/tot_size)*100<<std::endl;
    std::cout<<"symbol runs ss: "<<(sdsl::size_in_mega_bytes(m_symbol_runs_ss)/tot_size)*100<<std::endl;
    std::cout<<"symbol runs sums: "<<(sdsl::size_in_mega_bytes(m_symbol_runs_sums)/tot_size)*100<<std::endl;
    std::cout<<"symbol runs sums rs: "<<(sdsl::size_in_mega_bytes(m_symbol_runs_sums_rs)/tot_size)*100<<std::endl;
    std::cout<<"symbol runs sums ss: "<<(sdsl::size_in_mega_bytes(m_symbol_runs_sums_ss)/tot_size)*100<<std::endl;
}
