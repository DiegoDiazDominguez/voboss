//
// Created by diediaz on 27-11-18.
//

#include <easylogging++.h>
#include <iostream>
#include <fstream>
#include "vo_boss.hpp"

INITIALIZE_EASYLOGGINGPP

int main(int argc, char* argv[]) {

    double t_nc=0,t_bl=0,t_fo=0;
    clock_t begin, end;
    size_t n_samp=0;

    if(argc==1){
        std::cout<<"missing dbg index"<<std::endl;
        exit(1);
    }

    vo_boss vboss;
    std::string input_file(argv[1]);
    sdsl::load_from_file(vboss, input_file);

    std::random_device rd; // obtain a random number from hardware
    std::mt19937 eng(rd()); // seed the generator
    std::uniform_int_distribution<size_t> distr(1, vboss.solid_kmers); // define the range
    std::vector<size_t> trans_nodes(vboss.k);
    std::vector<uint8_t> label(vboss.k);
    size_t k,l;
    vo_boss::degree_t ind;

    /*vo_boss::node_t tmp = {29611913,29611913,vboss.k-1};
    std::cout<<vboss.node2string(29611907,true)<<std::endl;
    std::cout<<vboss.node2string(29611908,true)<<std::endl;
    std::cout<<vboss.node2string(29611909,true)<<std::endl;
    std::cout<<vboss.node2string(29611910,true)<<std::endl;
    std::cout<<vboss.node2string(29611911,true)<<std::endl;
    std::cout<<vboss.node2string(29611912,true)<<std::endl;
    std::cout<<vboss.node2string(29611913,true)<<std::endl;
    std::cout<<vboss.buildL(tmp).size()<<std::endl;
    vboss.foverlaps(tmp);*/

    for(size_t i=0;i<1000;i++){

        size_t solid_node = vboss.solid_nodes_ss(distr(eng));
        std::pair<size_t, bool> outd = vboss.dbg_outdegree(solid_node);

        vo_boss::node_t tmp_node = {solid_node, solid_node, vboss.k-1};

        if((outd.first-outd.second)==0) continue;
        if(vboss.next_contained(tmp_node)==vboss.root()) continue ;

        begin = clock();
        vboss.next_contained(tmp_node);
        end = clock();
        t_nc+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        vboss.buildL(tmp_node);
        end = clock();
        t_bl+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        vboss.foverlaps(tmp_node);
        end = clock();
        t_fo+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        n_samp++;
    }

    std::cout<<"#File_name\tnext_contained(usecs)\tbuildL(usecs)\tfoverlaps(usecs)"<<std::endl;
    std::cout<<input_file<<"\t"
    <<t_nc/n_samp<<"\t"
    <<t_bl/n_samp<<"\t"
    <<t_fo/n_samp<<"\t"<<std::endl;

    return 0;
}

